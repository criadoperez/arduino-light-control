#include <Time.h>
//#include <JeeLib.h> //Include library containing low power functions


const int relayOne =  7;      // the number of the Relay pin
const int relayTwo = 4;

// Variables will change:
int ledState;             // ledState used to set the LED
int sunriseHour;
int sunsetHour;

void setup() {
  pinMode(relayOne, OUTPUT);     //Declares pin as an output
  pinMode(relayTwo, OUTPUT);     //Declares pin as an output
  setTime(13,11,0,29,6,2014);     //Set Initial time setTime(hours, minutes, seconds, days, months, years); If you are setting the clock in summer you need to put an hour less than what it really is. In winter just put actual time.
  //Serial.begin();
}

void loop()
{
  
  // if(month()==1) { sunriseHour=8; sunsetHour=18;}
   //if(month()==12) {}
  //Set sunrise and sunset times according to the month  
   switch(month()) {
     case 1:
       sunriseHour=8;
       sunsetHour=18;
       break;
     case 2:
       sunriseHour=8;
       sunsetHour=18;
       break;
     case 3: //TIME CHANGES IN MARCH SO THIS IS NOT THE POLITICAL TIME ANYMORE
       sunriseHour=7; //Real time is one hour after this.
       sunsetHour=19;
       break;
     case 4:  //TIME CHANGES IN MARC
       sunriseHour=7;
       sunsetHour=20;
       break;
     case 5:
       sunriseHour= 6;
       sunsetHour=20;
       break;
     case 6:
       sunriseHour=5;
       sunsetHour=20;
       break;
     case 7:
       sunriseHour=5;
       sunsetHour=20;
       break;
     case 8:
       sunriseHour=6;
       sunsetHour=20;
       break;
     case 9:
       sunriseHour=6;
       sunsetHour=19;
       break;
     case 10: //Time changes again this month, so we are back to normal
       sunriseHour=7;
       sunsetHour=18;
       break;
     case 11:
       sunriseHour=7;
       sunsetHour=18;
       break;
     case 12:
        sunriseHour=8;
        sunsetHour=17;
       break;
   }
   
    if (hour() >= sunriseHour && hour()<=sunsetHour){ //Light will be off from Sunrise to one hour after Sunset because its <=
      ledState = HIGH; //HIGH is lights off
  }
  else { ledState = LOW;} //LOW is lights on :-)
  digitalWrite(relayOne, ledState);
  digitalWrite(relayTwo, ledState);
  delay(60000); //Delay in ms. It will execute the loop once a minute
}


