# Arduino Light Control

Originally written in June 2014 for an [Arduino](https://www.arduino.cc/) [UNO](https://store.arduino.cc/products/arduino-uno-rev3).

Arduino code to control outdoor lights at home.
It checks the time once a minute and will turn on/off the lights accordingly.
You can setup a different on/off time of every month of the year.

![](https://upload.wikimedia.org/wikipedia/commons/1/1c/Arduino-uno.jpg)
